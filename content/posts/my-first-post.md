---
title: "Aunty M's First Post"
date: 2022-03-04T20:25:18-05:00
draft: false
---

Magnus Hirschfeld (14 May 1868 – 14 May 1935) was a German physician and sexologist educated primarily in Germany; he based his practice in Berlin-Charlottenburg during the Weimar period. An outspoken advocate for sexual minorities, Hirschfeld founded the Scientific-Humanitarian Committee and World League for Sexual Reform. Historian Dustin Goltz characterized the committee as having carried out "the first advocacy for homosexual and transgender rights".[2] "Hirschfeld's radical ideas changed the way Germans thought about sexuality."[3] Hirschfeld was targeted by Nazis for being Jewish and gay; he was beaten by völkisch activists in 1920, and in 1933 his Institut für Sexualwissenschaft was sacked and had its books burned by Nazis. He was forced into exile in France, where he died in 1935. 
