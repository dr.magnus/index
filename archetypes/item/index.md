---
title: "{{ replace .Name "-" " " | title }}"
originalyear: 1000					# ✏️ when was it published

tags:								# ✏️
#  - Internet Archive
#  - re-OCR
#  - PDF
#lang: en							# ✏️

draft: true	# ✏️ page will not be included in site build until draft status is changed

date: {{ .Date }}
lastmod: {{ .Date }}
---

## Summary

{{< readfile "info-summary.md" >}}

## Item information

Year of original publication: {{< param originalyear >}}


### Provenance of item

{{< readfile "info-provenance.md" >}}

### Notes on modifications and changes made	

{{< readfile "info-changes.md" >}}

## Commentary

{{< readfile "commentary.md" >}}

## Full text

{{< readfile "item-fulltext.md" >}}

## See also

none












